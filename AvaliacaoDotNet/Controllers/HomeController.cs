﻿using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvaliacaoDotNet.Controllers
{
    public class HomeController : BusinessLogic.Security.AvaliacaoDotNetBaseController
    {
        [MvcSiteMapNode(Title = "Home", Key = "home")]
        public ActionResult Home()
        {
            return View();
        }


        [MvcSiteMapNode(Title = "Dashboard", ParentKey = "home" , Key = "dashboard", Order = 1, ImageUrl = "fa fa-dashboard")]
        public ActionResult Index()
        {
            return View();
        }
    }
}