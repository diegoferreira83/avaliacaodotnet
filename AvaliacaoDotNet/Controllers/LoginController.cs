﻿using AvaliacaoDotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvaliacaoDotNet.Controllers
{
    public class LoginController : Controller
    {
        private AvaliacaoDotNetEntities db = new AvaliacaoDotNetEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserLogin userLogin)
        {
            if (ModelState.IsValid)
            {
                BusinessLogic.Security.AccessControl accessControl = new BusinessLogic.Security.AccessControl(db);

                if (accessControl.SignOn(userLogin.Username, userLogin.Password))
                    return RedirectToAction("Index", "Home", new { area = "" });

                ModelState.AddModelError("", "Usuário ou senha inválida");
            }

            return View();
        }

        public ActionResult Logout()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Clear();
            return RedirectToAction("Index", "Login", new { area = "" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}