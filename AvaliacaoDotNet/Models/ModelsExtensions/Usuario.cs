﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoDotNet.Models
{
    public partial class Usuario
    {
        /// <summary>
        /// Returns the salt used to calculate the password hash (Got from Password property).
        /// </summary>
        public byte[] Salt
        {
            get
            {
                byte[] salt = null;
                if (this.strSenha != null)
                {
                    byte[] saltAndHash = System.Convert.FromBase64String(this.strSenha);
                    salt = new byte[48];
                    Array.Copy(saltAndHash, 0, salt, 0, 48);
                }

                return salt;
            }
        }

        /// <summary>
        /// Returns the password hash (Got from Password property).
        /// </summary> 
        public byte[] Hash
        {
            get
            {
                byte[] hash = null;
                if (this.strSenha != null)
                {
                    byte[] saltAndHash = System.Convert.FromBase64String(this.strSenha);
                    hash = new byte[saltAndHash.Length - 48]; // 48 is the size of salt
                    Array.Copy(saltAndHash, 48, hash, 0, hash.Length);
                }

                return hash;
            }
        }

        /// <summary>
        /// Calculates the password salted hash and store it as Base64 string at Password property.
        /// </summary>
        /// <param name="newpassword"></param>
        public void SetPassword(string newpassword)
        {
            byte[] salt = BusinessLogic.Security.AccessControl.RandomSalt;
            byte[] passHash = BusinessLogic.Security.AccessControl.GeneratePasswordHash(salt, newpassword);

            // concatenates the salt and hash in one vector
            byte[] finalData = new byte[salt.Length + passHash.Length];
            Array.Copy(salt, finalData, salt.Length);
            Array.Copy(passHash, 0, finalData, salt.Length, passHash.Length);

            this.strSenha = System.Convert.ToBase64String(finalData);
        }
    }
}
