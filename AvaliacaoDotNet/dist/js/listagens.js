﻿var TableManaged = function () {

    var initTable = function () {

        var table = $('#listagem');

        $.extend($.fn.dataTable.defaults, {
            "searching": true,
            "ordering": true
        });

        // begin first table
        table.dataTable({
            "lengthMenu": [
                [15, 20, 50, -1],
                [15, 20, 50, "Todos"] // change per page values here
            ],
            // set the initial value
            "pageLength": 15,
            "pagingType": "full_numbers",
            "language": {
                "lengthMenu": "Exibindo _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro cadastrado",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Próximo",
                    "last": "Último",
                    "first": "Primeiro"
                },
                "emptyTable": "No data available in table",
                "thousands": ".",
                "decimal": ",",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "search": "Busca:"
            },
            "columnDefs": [{  // set default column settings
                'orderable': true
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#listagem_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable();
        }

    };

}();