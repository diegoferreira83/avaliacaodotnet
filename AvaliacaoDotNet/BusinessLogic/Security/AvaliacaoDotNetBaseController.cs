﻿using AvaliacaoDotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AvaliacaoDotNet.BusinessLogic.Security
{
    public class AvaliacaoDotNetBaseController : Controller
    {
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (AccessControl.IsAuthenticated)
            {
                filterContext.HttpContext.User =
                    new CustomPrincipal(
                        new CustomIdentity(
                            AccessControl.Username));
            }

            base.OnAuthorization(filterContext);
        }

        public AvaliacaoDotNetBaseController()
        {
            using (var db = new AvaliacaoDotNetEntities())
            {
                ViewBag.Usuario = db.Usuarios.FirstOrDefault(u => u.idUsuario == AccessControl.UserID);
            }
        }
    }
}
