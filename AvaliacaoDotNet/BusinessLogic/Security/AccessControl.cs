﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using AvaliacaoDotNet.Models;

namespace AvaliacaoDotNet.BusinessLogic.Security
{
    public class AccessControl
    {
        private readonly AvaliacaoDotNetEntities  _context;

        public AccessControl(AvaliacaoDotNetEntities context) { _context = context; }

        /// <summary>
        /// Return a 48 byte ramdom salt.
        /// </summary>
        static public byte[] RandomSalt
        {
            get
            {
                byte[] salt = new byte[48];
                using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
                    rngCsp.GetBytes(salt);
                return salt;
            }
        }

        static private bool ByteArraysEqual(byte[] b1, byte[] b2)
        {
            if (b1 == b2) return true;
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i]) return false;
            }
            return true;
        }

        /// <summary>
        /// Check if user exists at database and verify the password against the user hashed password stored at database.
        /// </summary>
        /// <param name="username">Login name</param>
        /// <param name="password">User password to validate</param>
        /// <param name="name">Receives the full user name</param>
        /// <returns>Return true if user is ok.</returns>
        public bool SignOn(string username, string password)
        {
            if (_context == null) throw new InvalidOperationException();

            var user = (from u in _context.Usuarios
                        where u.strEmail == username && u.bolAtivo == true
                        select u).FirstOrDefault();

            if (user != null)
            {
                byte[] pwdHash = GeneratePasswordHash(user.Salt, password);
                if (ByteArraysEqual(pwdHash, user.Hash))
                {
                    bool isAdm = true;

                    System.Web.Security.FormsAuthenticationTicket ticket = new System.Web.Security.FormsAuthenticationTicket(1,
                      user.idUsuario.ToString("D8") + "#" + username,
                      DateTime.Now,
                      DateTime.Now.AddMinutes(15),
                      false,
                      isAdm ? "#" + user.strNome : user.strNome,
                      System.Web.Security.FormsAuthentication.FormsCookiePath);

#if DEBUG
                    System.Diagnostics.Debugger.Log(0, "SEC", "User " + username + " logged in at " + ticket.IssueDate.ToString());
#endif
                    // Encrypt the ticket.
                    string encTicket = System.Web.Security.FormsAuthentication.Encrypt(ticket);

                    HttpContext.Current.Response.Cookies.Add(new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encTicket));
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns a hash for a password given a salt value.
        /// </summary>
        /// <param name="salt">The salt value.</param>
        /// <param name="password">The password to generate hash.</param>
        /// <returns>SHA256 password hash SHA256(salt + password).</returns>
        static public byte[] GeneratePasswordHash(byte[] salt, string password)
        {
            Byte[] bytes;
            using (SHA256 hasher = SHA256.Create())
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                bytes = encoding.GetBytes(password);

                hasher.TransformBlock(salt, 0, salt.Length, salt, 0);
                hasher.TransformFinalBlock(bytes, 0, bytes.Length);

                bytes = hasher.Hash;
            }

            return bytes;
        }

        static public bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.Request.IsAuthenticated;
            }
        }

        static public string FullUsername
        {
            get
            {
                HttpCookie secCookie = HttpContext.Current.Request.Cookies.Get(System.Web.Security.FormsAuthentication.FormsCookieName);
                System.Web.Security.FormsAuthenticationTicket ticket =
                    System.Web.Security.FormsAuthentication.Decrypt(secCookie.Value);
                //if (ticket.UserData[0] == '#')
                //    return ticket.UserData.Remove(0, 1);

                return ticket.Name;
            }
        }

        static public int UserID
        {
            get
            {
                HttpCookie secCookie = HttpContext.Current.Request.Cookies.Get(System.Web.Security.FormsAuthentication.FormsCookieName);
                System.Web.Security.FormsAuthenticationTicket ticket =
                    System.Web.Security.FormsAuthentication.Decrypt(secCookie.Value);
                if (ticket.Name.Length < 9 || ticket.Name[8] != '#')
                    return -1;

                return int.Parse(ticket.Name.Substring(0, 8), System.Globalization.NumberStyles.HexNumber);
            }
        }

        static public string Username
        {
            get
            {
                HttpCookie secCookie = HttpContext.Current.Request.Cookies.Get(System.Web.Security.FormsAuthentication.FormsCookieName);
                System.Web.Security.FormsAuthenticationTicket ticket =
                    System.Web.Security.FormsAuthentication.Decrypt(secCookie.Value);
                if (ticket.Name.Contains("#"))
                    return ticket.Name.Split('#')[1];

                return ticket.Name.Remove(0, 9);
            }
        }

        static public bool IsAdmin
        {
            get
            {
                HttpCookie secCookie = HttpContext.Current.Request.Cookies.Get(System.Web.Security.FormsAuthentication.FormsCookieName);
                System.Web.Security.FormsAuthenticationTicket ticket =
                    System.Web.Security.FormsAuthentication.Decrypt(secCookie.Value);
                return ticket.UserData[0] == '#';
            }
        }
    }
}