﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvaliacaoDotNet.BusinessLogic
{
    public class ExceptionHandling
    {
        public static string GetExceptionMessage(Exception ex)
        {
            Exception inner = ex.InnerException;
            while (inner != null)
            {
                System.Data.SqlClient.SqlException sqlException = inner as System.Data.SqlClient.SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2627: // primary key or unique constraint violation
                        case 2601: // unique index violation
                            return "Objeto já existe";
                        case 547:  // foreign key dependency
                            return "Violação de Chave Estrangeira";
                    }

                    return inner.Message;
                }

                inner = inner.InnerException;
            }

            return ex.Message;
        }
    }
}